package cloudintegration.dropbox;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cloudintegration.dropbox.util.DropboxUtils;
import cloudintegration.sharedobjects.FileInfo;
import cloudintegration.sugarsync.SugarSync;
import cloudintegration.sugarsync.util.HttpResponse;
import cloudintegration.sugarsync.util.SugarSyncUtils;
import cloudintegration.sugarsync.util.UserInfo;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Account;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.RequestTokenPair;
import com.dropbox.client2.session.Session;
import com.dropbox.client2.session.Session.AccessType;
import com.dropbox.client2.session.WebAuthSession;
import com.dropbox.client2.session.WebAuthSession.WebAuthInfo;

public class DropBoxFacade {
	
	
	final static private String APP_KEY 		= "e70uzy7w27umtsr";
	final static private String APP_SECRET 		= "arzq9d0pcersnjr";
	final static private AccessType ACCESS_TYPE = AccessType.DROPBOX;
	final static private AppKeyPair APP_KEYS 	= new AppKeyPair(APP_KEY, APP_SECRET);
	private static DropboxAPI<Session> mDBApi;


	/**
	 * Authenticates this application to use the given users drop box. This is done only once.
	 * 
	 * @return a String containing the URL that authorises the apps access to the users dropbox
	 * @throws DropboxException
	 */
	public static String authenticateApp(DropBox dropBox) throws DropboxException {
				
		
		WebAuthSession session = new WebAuthSession(APP_KEYS, ACCESS_TYPE);
		//mDBApi = new DropboxAPI<Session>(session);
		
		WebAuthInfo webAuthInfo = session.getAuthInfo("http://localhost:8000/CloudIntegration/login?cloudStorageName=dropbox");
		
		dropBox.setWebAuthInfo(webAuthInfo);
		dropBox.setSession(session);
				
		return webAuthInfo.url;
			
	}
	
	
	
	
	
	public static boolean validateLogin(DropBox dropBox) {
		
		boolean success = false;
		try {
			WebAuthInfo webAuthInfo = dropBox.getWebAuthInfo();
			WebAuthSession session = dropBox.getSession();
			
			String accessToken = session.retrieveWebAccessToken(webAuthInfo.requestTokenPair);
	        AccessTokenPair accessTokenPair = session.getAccessTokenPair();
	        	
	        DropboxAPI<?> dropboxApi = new DropboxAPI<WebAuthSession>(session);
	        
	        dropBox.setDropboxApi(dropboxApi);
				
	        success = true;
	        //Account account = dropboxApi.accountInfo();
		} catch (DropboxException exDrop){
			
		}
        
		return success;				
	}
	

	
	/**
	 * Fetches the root/home directory content.
	 * 
	 * @param dropBox the cloud storage
	 */
	static void fetchHomeContent(DropBox dropBox) {
				
		DropboxAPI<?> dropboxApi = dropBox.getDropboxApi();
		
		try {
					
			String parent = "dropbox";
			Entry entry = dropboxApi.metadata("", 10000, null, true, null);
			
			List<Entry> listContents = entry.contents;
						
			Map<String, FileInfo> homeContents = DropboxUtils.getContentsRepresentationAsMap(listContents, true);
					
			dropBox.setHomeContents(homeContents);
			
		} catch (DropboxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}




	/**
	 * Gets the next level in the directory tree based on the parent (contentLink) for the given 
	 * cloud storage.
	 * 
	 * @param contentLink the parent
	 * @param dropBox the cloud storage
	 * @return a map of the files in the next level
	 */
	public static Map<String, FileInfo> getNextLevel(String contentLink, DropBox dropBox) {
		DropboxAPI<?> dropboxApi = dropBox.getDropboxApi();
		Map<String, FileInfo> contents = new HashMap<String, FileInfo>();	
		
		try {
					
			Entry entry = dropboxApi.metadata(contentLink, 10000, null, true, null);
			
			List<Entry> listContents = entry.contents;
						
			contents = DropboxUtils.getContentsRepresentationAsMap(listContents, true);
			
		} catch (DropboxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return contents;
	}
	
	
	
	
	
}
