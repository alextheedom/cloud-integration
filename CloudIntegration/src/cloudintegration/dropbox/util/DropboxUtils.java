package cloudintegration.dropbox.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dropbox.client2.DropboxAPI.Entry;

import cloudintegration.sharedobjects.FileInfo;
import cloudintegration.sharedobjects.ImageManagement;

public class DropboxUtils {


	
	/**
	 * Parses the List returned from a directory resource request.
	 * 
	 * @param listContents	a list of the files 
	 * @param fileDetails	if true file details will be extracted.
	 * @return a map of the files and the files metadata.
	 */
	public static Map<String, FileInfo> getContentsRepresentationAsMap(List<Entry> listContents, boolean fileDetails) {	
		
		Map<String, FileInfo> contents = new HashMap<String, FileInfo>();	
		
		String defaultIcon = ImageManagement.getImage("file");	
		
		for(Entry e : listContents){
			
			FileInfo fileInfo = new FileInfo();
			
			String displayName = e.fileName();

			fileInfo.setContentsLink(e.path);
			fileInfo.setDisplayName(displayName);
			//fileInfo.setBytes(e.bytes);
			fileInfo.setClientMtime(e.clientMtime);
			fileInfo.setContents(e.contents);	
			fileInfo.setHash(e.hash);
			fileInfo.setDeleted(e.isDeleted);	
			fileInfo.setDir(e.isDir);	
			fileInfo.setMimeType(e.mimeType);	
			fileInfo.setLastModified(e.modified);
			fileInfo.setPath(e.path);	
			fileInfo.setRev(e.rev);
			fileInfo.setRoot(e.root);	
			fileInfo.setSize(e.size);	
			fileInfo.setThumbExists(e.thumbExists);		
			
			String icon = e.mimeType == null ? defaultIcon  : ImageManagement.getImageBasedOnMime(e.mimeType);			
			fileInfo.setIcon(icon);
			
			contents.put(displayName, fileInfo);
			
		}
			
              
		return contents;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
