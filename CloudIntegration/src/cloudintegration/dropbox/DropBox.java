package cloudintegration.dropbox;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.WebAuthSession;
import com.dropbox.client2.session.WebAuthSession.WebAuthInfo;

import cloudintegration.CloudStorage;
import cloudintegration.sharedobjects.FileInfo;
import cloudintegration.sugarsync.SugarSyncFacade;
import cloudintegration.sugarsync.util.HttpResponse;
import exceptions.InvalidLoginException;

public class DropBox implements CloudStorage {

	private static final String NAME = "DropBox";
	
	private WebAuthInfo webAuthInfo;
	private WebAuthSession session;
	private DropboxAPI<?> dropboxApi;
	private String accessToken;
	private HttpResponse currentHttpResponse;
	private Map<String, FileInfo> homeContents 		= new HashMap <String, FileInfo> ();
	private Map<String, FileInfo> currentContents 	= new HashMap <String, FileInfo> ();
	private Logger logger = null;
	
	{
		logger = Logger.getRootLogger();
		BasicConfigurator.configure();
		logger.setLevel(Level.DEBUG);
	}
	



	/**
	 * Authenticates this app to use the given users dropbox
	 */
	public String authenticateApp() throws Exception {
		String url = DropBoxFacade.authenticateApp(this);

		return url;
	}
	
	
	
	/**
	 * Attempts to login to this cloud storage with the given username and password.
	 */
	public CloudStorage login(String username, String password) throws InvalidLoginException{
			
		boolean success = DropBoxFacade.validateLogin(this);

		if (!success) {
			logger.debug("Login was unsuccessful.");
			throw new InvalidLoginException();
		}
		
		return this;
	}

	
	
	/**
	 * Obtains the home directory of this cloud storage
	 */
	public Map<String, FileInfo> getHomeDirectory() {
		logger.debug("Enter Dropbox getHomeDirectory method");
		
		DropBoxFacade.fetchHomeContent(this);
		
		logger.debug("Exit Dropbox getHomeDirectory method");
		
		return getHomeContents();
	}
	
	

	/**
	 * Obtains the next level in the directory based on the parent (contentLink)
	 */
	public Map<String, FileInfo> getNextLevel(String contentLink) throws IOException {	
		logger.debug("Enter DropBox getNextLevel method");
		
		Map<String, FileInfo> nextLevel = DropBoxFacade.getNextLevel(contentLink, this);

		logger.debug("Enter DropBox getNextLevel method");
		return nextLevel;		
	}

	

	/**
	 * Returns the name of this cloud storage
	 */
	public String getName() {
		return this.NAME;
	}






	@Override
	public FileInfo getFile(String displayName, String path) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public InputStream getInputStream(String displayName) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}



	public WebAuthInfo getWebAuthInfo() {
		return webAuthInfo;
	}



	public void setWebAuthInfo(WebAuthInfo webAuthInfo) {
		this.webAuthInfo = webAuthInfo;
	}



	public WebAuthSession getSession() {
		return session;
	}



	public void setSession(WebAuthSession session) {
		this.session = session;
	}



	public DropboxAPI<?> getDropboxApi() {
		return dropboxApi;
	}



	public void setDropboxApi(DropboxAPI<?> dropboxApi) {
		this.dropboxApi = dropboxApi;
	}

	
	Map<String, FileInfo> getHomeContents() {
		if(this.homeContents == null || this.homeContents.isEmpty()){
			DropBoxFacade.fetchHomeContent(this);
		}
		return homeContents;
	}



	void setHomeContents(Map<String, FileInfo> homeContents) {		
		this.homeContents = homeContents;
	}


}
