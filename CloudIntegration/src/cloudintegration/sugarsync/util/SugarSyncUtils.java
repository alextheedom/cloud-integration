package cloudintegration.sugarsync.util;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cloudintegration.sharedobjects.FileInfo;
import cloudintegration.sharedobjects.ImageManagement;
import cloudintegration.sharedobjects.Utils;
import cloudintegration.sugarsync.util.UserInfo;

import java.text.DateFormat;
import java.math.RoundingMode;

public class SugarSyncUtils {


	/**
	 * Returns an HttpResponse object of the folder representation.
	 * 
	 * @param accessToken
	 * @param folderName
	 * @return
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public static HttpResponse getSyncFolderRepresentation(String accessToken, String folderName) throws IOException, XPathExpressionException {
        
    	HttpResponse userInfoResponse = getUserInfo(accessToken);

        // get the folder representation link
        String folderLink = XmlUtil.getNodeValues(userInfoResponse.getResponseBody(), "/user/" + folderName + "/text()").get(0);

        // make a HTTP GET to the link extracted from user info
        HttpResponse folderRepresentationResponse = SugarSyncHTTPGetUtil.getRequest(folderLink, accessToken);
        validateHttpResponse(folderRepresentationResponse);

        return folderRepresentationResponse;
    }
	
	
	
	/**
	 * Retruns an HttpResponse respresentation of the folder and file structure.
	 * @param accessToken
	 * @param folderName
	 * @return
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public static HttpResponse getContentsRepresentation(String accessToken, String collectionType, String displayName, String responseBody) throws IOException, XPathExpressionException {
        System.out.println(responseBody);
		// Search for the folder name in the responseBody XML		
        List<String> contentsLink = XmlUtil.getNodeValues(responseBody,"/collectionContents/collection[@type=\"r1\" and displayName=\"r2\"]/contents/text()".replace("r1", collectionType).replace("r2", displayName));        
		// make a GET request for the info.
        HttpResponse httpResponse = SugarSyncHTTPGetUtil.getRequest(contentsLink.get(0), accessToken);
        validateHttpResponse(httpResponse);

        return httpResponse;
    }
	
    /**
     * Prints the files and folders from the xml response
     * 
     * @param responseBody
     *            the xml server response
     */
    public static void printFolderContents(String responseBody, String nodeType) {
        try {
            List<String> folderNames = XmlUtil.getNodeValues(responseBody,"/collectionContents/collection[@type=\"folder\"]/displayName/text()".replace("folder", nodeType));
            List<String> fileNames = XmlUtil.getNodeValues(responseBody,"/collectionContents/file/displayName/text()");
            System.out.println("\n-"+nodeType);
            System.out.println("\t-Folders:");
            for (String folder : folderNames) {
                System.out.println("\t\t" + folder);
            }
            System.out.println("\t-Files:");
            for (String file : fileNames) {
                System.out.println("\t\t" + file);
            }
        } catch (XPathExpressionException e1) {
            System.out.println("Error while printing the folder contents:");
            System.out.println(responseBody);
        }
    }
	
    /**
     * Returns the account information
     * 
     * @param accessToken
     *            the access token
     * @return a HttpResponse containing the server's xml response in the
     *         response body
     * @throws IOException
     *             if any I/O error occurs
     */
    public static HttpResponse getUserInfo(String accessToken) throws IOException {
        HttpResponse httpResponse = UserInfo.getUserInfo(accessToken);
        validateHttpResponse(httpResponse);
        return httpResponse;
    }

    /**
     * Validates the HTTP response. If HTTP response status code indicates an
     * error the details are printed and the tool exists
     * 
     * @param httpResponse
     *            the HTTP response which will be validated
     */
    public static void validateHttpResponse(HttpResponse httpResponse) {
        if (httpResponse.getHttpStatusCode() > 299) {
            System.out.println("HTTP ERROR!");
            printResponse(httpResponse);
            System.exit(0);
        }
    }
    
    public static void printResponse(HttpResponse response) {
        System.out.println("STATUS CODE: " + response.getHttpStatusCode());
        // if the response is in xml format try to pretty format it, otherwise
        // leave it as it is
        String responseBodyString = null;
        try {
            responseBodyString = XmlUtil.formatXml(response.getResponseBody());
        } catch (Exception e) {
            responseBodyString = response.getResponseBody();
        }
        System.out.println("RESPONSE BODY:\n" + responseBodyString);
    }

    
	public static void printHttpResponse(HttpResponse authResponse) throws TransformerException {
		System.out.println("status code: " + authResponse.getHttpStatusCode());
		System.out.println("Response body: " + XmlUtil.formatXml(authResponse.getResponseBody()));
					
		for(Header header : authResponse.getHeaders()){
			System.out.println("Name = " + header.getName() + " : value = " + header.getValue() );				
		}
	}


	public static void main(String... args){
		
//		Map<String, String> contents = SugarSyncUtils.getUserInfoAsMap();
//		
//		for(String displayName : contents.keySet()){
//			System.out.println(displayName + " - " + contents.get(displayName));
//		}
		
	}
	
	
	
	public static Map<String, FileInfo> getUserInfoAsMap(HttpResponse httpResponse) {	
		//public static Map<String, String> getUserInfoAsMap() {	
		
		Map<String, FileInfo> contents = new HashMap<String, FileInfo>();
		String responseBody = httpResponse.getResponseBody();	
		//String responseBody = "<user><username>alex.theedom@gmail.com</username><nickname>alextheedom</nickname><salt>nFIQQQ==</salt><quota><limit>32749125632</limit><usage>27731390749</usage></quota><workspaces>https://api.sugarsync.com/user/3786590/workspaces/contents</workspaces><syncfolders>https://api.sugarsync.com/user/3786590/folders/contents</syncfolders><deleted>https://api.sugarsync.com/folder/:sc:3786590:9</deleted><magicBriefcase>https://api.sugarsync.com/folder/:sc:3786590:2</magicBriefcase><webArchive>https://api.sugarsync.com/folder/:sc:3786590:1</webArchive><mobilePhotos>https://api.sugarsync.com/folder/:sc:3786590:3</mobilePhotos><albums>https://api.sugarsync.com/user/3786590/albums/contents</albums><recentActivities>https://api.sugarsync.com/user/3786590/recentActivities/contents</recentActivities><receivedShares>https://api.sugarsync.com/user/3786590/receivedShares/contents</receivedShares><publicLinks>https://api.sugarsync.com/user/3786590/publicLinks/contents</publicLinks><maximumPublicLinkSize>-1</maximumPublicLinkSize></user>";
		System.out.println(responseBody);
		
		try {
			NodeList nodeList = XmlUtil.getNodeList(responseBody,"/user");
			
			String displayName = "";
			String contentsLink = "";
			
			// Get the displayname and the content link
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node collection = nodeList.item(i);
				NodeList childNodes = collection.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++){
					Node node = childNodes.item(y);
					String nodeName = node.getNodeName();
					FileInfo fileInfo = new FileInfo();
					
					switch (nodeName){
						case "workspaces" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "syncfolders" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "deleted" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "magicBriefcase" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "webArchive" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "mobilePhotos" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "albums" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "recentActivities" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "receivedShares" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
						case "publicLinks" :
							contentsLink = node.getTextContent();
							fileInfo.setContentsLink(contentsLink);
							fileInfo.setDisplayName(nodeName);
							fileInfo.setIcon(ImageManagement.getImage(nodeName));
							contents.put(nodeName, fileInfo);
						break;
							
					}
					
				}
			}
				
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
      
        
        
        
		return contents;
		
	}
	
	
	/**
	 * Parses the XML return from a directory resource request.
	 * 
	 * @param httpResponse	the response from the server
	 * @param fileDetails	if true file details will be extracted.
	 * @return
	 */
	public static Map<String, FileInfo> getContentsRepresentationAsMap(HttpResponse httpResponse, boolean fileDetails) {	
		
		Map<String, FileInfo> contents = new HashMap<String, FileInfo>();		
		String responseBody = httpResponse.getResponseBody();
		
		System.out.println(responseBody);
		
		try {
			
			NodeList nodeListCollection = XmlUtil.getNodeList(responseBody, "/collectionContents/collection");		
			getCollectionContents(nodeListCollection, contents);
			
			NodeList nodeListFile = XmlUtil.getNodeList(responseBody, "/collectionContents/file");		
			getFileContents(nodeListFile, contents, fileDetails);
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
                  
		return contents;
	}

	

	/**
	 * Parses the XML respresentation of the file and puts it in a map.
	 * 	
	 * <file>
			<displayName>sync.ffs_lock</displayName>
			<ref>https://api.sugarsync.com/file/:sc:3786590:14138710_350512</ref>
			<size>4322</size>
			<lastModified>2012-12-09T14:48:01.000-08:00</lastModified>
			<mediaType>application/octet-stream</mediaType>
			<presentOnServer>true</presentOnServer>
			<fileData>https://api.sugarsync.com/file/:sc:3786590:14138710_350512/data</fileData>
		</file>
	 * 
	 * @param nodeListFile	XML representation of the file info
	 * @param contents		the map in which the file info will be put
	 * @param fileDetails	true if file details such as size should be included in the dile info object
	 */
	private static void getFileContents(NodeList nodeListFile, Map<String, FileInfo> contents, boolean fileDetails) {
		
		String displayName = "";
		String contentsLink = "";
		String fileSize = "";
		String lastModified = "";
		String mediaType = "";
		String ref = "";

		for (int i = 0; i < nodeListFile.getLength(); i++) {

			FileInfo fileInfo = new FileInfo();
			Node collection = nodeListFile.item(i);
			NodeList childNodes = collection.getChildNodes();
			String icon = ImageManagement.getImage("file");
			for (int y = 0; y < childNodes.getLength(); y++){
				
				Node node = childNodes.item(y);
				String nodeName = node.getNodeName();
				if("displayName".equalsIgnoreCase(nodeName)){
					displayName = node.getTextContent();
					fileInfo.setDisplayName(displayName);
				} else if ("contents".equalsIgnoreCase(nodeName)){
					contentsLink = node.getTextContent();
					fileInfo.setContentsLink(contentsLink);
				} else if ("fileData".equalsIgnoreCase(nodeName)){
					contentsLink = node.getTextContent();
					fileInfo.setContentsLink(contentsLink);
				} else if ("mediaType".equalsIgnoreCase(nodeName)){
					mediaType = node.getTextContent();
					fileInfo.setMimeType(mediaType);				
					icon = ImageManagement.getImageBasedOnMime(mediaType);				
				} else if ("ref".equalsIgnoreCase(nodeName)){
					ref = node.getTextContent();
					fileInfo.setRef(ref);
				} else if (fileDetails && "size".equalsIgnoreCase(nodeName)){
					fileSize = Utils.convertFileSize(node.getTextContent());
					fileInfo.setSize(fileSize);
				} else if (fileDetails && "lastModified".equalsIgnoreCase(nodeName)){
					lastModified = " - " + node.getTextContent().substring(0, 10);
					fileInfo.setLastModified(lastModified);
				}
			}

			fileInfo.setIcon(icon);
			contents.put(displayName, fileInfo);
		}
		
	}


	
/**
 * Parses the XML representation of the folders and puts it in a map.
 * 
 * 	<collection type="folder">
		<displayName>Informes Valdepenas</displayName>
		<ref>https://api.sugarsync.com/folder/:sc:3786590:14138710_201610</ref>
		<contents>https://api.sugarsync.com/folder/:sc:3786590:14138710_201610/contents</contents>
	</collection>
	
 * @param nodeListCollection	XML representation of the folder info
 * @param contents				the map in which the file info will be put
 */
	private static void getCollectionContents(NodeList nodeListCollection, Map<String, FileInfo> contents) {
		
		String displayName = "";
		String contentsLink = "";

		for (int i = 0; i < nodeListCollection.getLength(); i++) {

			FileInfo fileInfo = new FileInfo();
			Node collection = nodeListCollection.item(i);
			
			NodeList childNodes = collection.getChildNodes();
			for (int y = 0; y < childNodes.getLength(); y++){
				
				Node node = childNodes.item(y);
				String nodeName = node.getNodeName();
				if("displayName".equalsIgnoreCase(nodeName)){
					displayName = node.getTextContent();
					fileInfo.setDisplayName(displayName);
				} else if ("contents".equalsIgnoreCase(nodeName)){
					contentsLink = node.getTextContent();
					fileInfo.setContentsLink(contentsLink);
				} 
				
			}
			
			fileInfo.setIcon(ImageManagement.getImage(collection.getAttributes().item(0).getNodeValue()));
			contents.put(displayName, fileInfo);
			
		}
			
		
	}






	/**
	 * Retrives the file from the sugar sync server and saves in on the local server then returns a FileInfo object
	 * about that file.
	 * 
	 * @param displayName 		the files name
	 * @param path 				the path on the server where the file will be saved  
	 * @param currentContents	a map respresentation of the current directory listing
	 * @param accessToken		the access token
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static FileInfo getFile(String displayName, String path, Map<String, FileInfo> currentContents, String accessToken) throws HttpException, IOException {
		
		FileInfo fileInfo = currentContents.get(displayName);
		
        InputStream in = SugarSyncUtils.getInputStream(displayName, currentContents, accessToken);
   
        File file = new File(path + "/" + displayName);
    
        FileOutputStream out = new FileOutputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
        in.close();
        out.close();   
        
		return fileInfo;
	}


	/**
	 * Opens an input stream to the file that we wish to download.
	 * 
	 * @param displayName 		the files name
	 * @param currentContents	a map representation of the current directory listing
	 * @param accessToken		the access token
	 * @return					returns an InputStream to the file requested.
	 * @throws HttpException
	 * @throws IOException
	 */
	public static InputStream getInputStream(String displayName, Map<String, FileInfo> currentContents, String accessToken) throws HttpException, IOException {
			
		FileInfo fileInfo = currentContents.get(displayName);		
		
		GetMethod get = SugarSyncHTTPGetUtil.executeGet(fileInfo.getContentsLink(), accessToken);
		
        InputStream in = get.getResponseBodyAsStream();  
        
        return in;
	}
}



















