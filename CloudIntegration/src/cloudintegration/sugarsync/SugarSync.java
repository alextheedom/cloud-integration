package cloudintegration.sugarsync;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import cloudintegration.CloudStorage;
import cloudintegration.sharedobjects.FileInfo;
import cloudintegration.sugarsync.util.HttpResponse;

import exceptions.InvalidLoginException;

public class SugarSync implements CloudStorage{
	
	private String accessToken;
	private HttpResponse currentHttpResponse;
	private Map<String, FileInfo> homeContents 		= new HashMap <String, FileInfo> ();
	private Map<String, FileInfo> currentContents 	= new HashMap <String, FileInfo> ();
	private Logger logger = null;
	private String name = "SugarSync";
	
	
	{
		logger = Logger.getRootLogger();
		BasicConfigurator.configure();
		logger.setLevel(Level.DEBUG);
	}
	
	
	
	/**
	 * Attempts to login to this cloud storage with the given username and password.
	 */
	public CloudStorage login(String username, String password) throws InvalidLoginException{
		logger.debug("Enter SugarSync login method");
		
		boolean success = SugarSyncFacade.validateLogin(username, password, this);

		if (!success) {
			logger.debug("Login was unsuccessful.");
			throw new InvalidLoginException();
		}
			
		logger.debug("Exit SugarSync login method");
		return this;
	}

	
	
	/**
	 * Obtains the home directory of this cloud storage
	 */
	public Map<String, FileInfo> getHomeDirectory() {
		logger.debug("Enter SugarSync getHomeDirectory method");
		
		SugarSyncFacade.fetchHomeContent(this);
		
		logger.debug("Exit SugarSync getHomeDirectory method");
		
		return getHomeContents();
	}
	
	
	
	/**
	 * Obtains the next level in the directory based on the parent (contentLink)
	 */
	public Map<String, FileInfo> getNextLevel(String contentLink) throws IOException {
		logger.debug("Enter SugarSync getNextLevel method");
		
		Map<String, FileInfo> nextLevel = SugarSyncFacade.getNextLevel(contentLink, this);


		logger.debug("Enter SugarSync getNextLevel method");
		return nextLevel;
	}
	
	

	/**
	 * Returns the name of this cloud storage
	 */
	public String getName() {
		return this.name;
	}







	@Override
	public FileInfo getFile(String displayName, String path) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public InputStream getInputStream(String displayName) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}



	void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}


	String getAccessToken(){
		return this.accessToken;
	}



	Map<String, FileInfo> getHomeContents() {
		if(this.homeContents == null || this.homeContents.isEmpty()){
			SugarSyncFacade.fetchHomeContent(this);
		}
		return homeContents;
	}



	void setHomeContents(Map<String, FileInfo> homeContents) {		
		this.homeContents = homeContents;
	}



	public Map<String, FileInfo> getCurrentContents() {
		return currentContents;
	}



	public void setCurrentContents(Map<String, FileInfo> currentContents) {
		this.currentContents = currentContents;
	}



	public HttpResponse getCurrentHttpResponse() {
		return currentHttpResponse;
	}



	public void setCurrentHttpResponse(HttpResponse currentHttpResponse) {
		this.currentHttpResponse = currentHttpResponse;
	}



	@Override
	public String authenticateApp() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	



	
	

	
}
