package cloudintegration.sugarsync;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import cloudintegration.sharedobjects.FileInfo;
import cloudintegration.sugarsync.auth.AccessToken;
import cloudintegration.sugarsync.auth.RefreshToken;
import cloudintegration.sugarsync.util.HttpResponse;
import cloudintegration.sugarsync.util.SugarSyncHTTPGetUtil;
import cloudintegration.sugarsync.util.SugarSyncUtils;
import cloudintegration.sugarsync.util.UserInfo;


public class SugarSyncFacade {

	private static final String LOCATION = "Location";
	private static final String PUBLIC_ACCESS_KEY = "Mzc4NjU5MDEzNTQ3NDg4MTM5MjA";
	private static final String PRIVATE_ACCESS_KEY = "YmRmMWU4YjIwMzA1NDQ0Yzk3OGNhMGY5OThhMjBiOGQ";
	private static final String APP_ID = "/sc/3786590/148_923557185";
	private static final String USERNAME = "alex.theedom@gmail.com";
	private static final String PASSWORD = "winter04";


	
	
	/**
	 * This method validates the user of sugarsync with the given username and password.
	 * 
	 * @param username the username
	 * @param password the password
	 * @param sugarSync the sugarSync object of the current user
	 * @return true if the login was successful otherwise false
	 */
	public static boolean validateLogin(String username, String password, SugarSync sugarSync) {
		boolean success = false;
		HttpResponse httpResponse = null;	
		System.out.println("getAuthorizationResponse:");
		try {
			httpResponse = RefreshToken.getAuthorizationResponse(username, password, APP_ID, PUBLIC_ACCESS_KEY, PRIVATE_ACCESS_KEY);
			sugarSync.setCurrentHttpResponse(httpResponse);
			String refreshToken = httpResponse.getHeader(LOCATION).getValue();

			System.out.println("getAccessTokenResponse:");
			httpResponse = AccessToken.getAccessTokenResponse(PUBLIC_ACCESS_KEY, PRIVATE_ACCESS_KEY, refreshToken);	
			sugarSync.setCurrentHttpResponse(httpResponse);
			System.out.println("getAccessTokenResponse:");
			String accessToken = httpResponse.getHeader(LOCATION).getValue();

			sugarSync.setAccessToken(accessToken);				
			success = true;
		} catch (IOException e) {
			// TODO: write error to log
		}

		return success;	

	}


	
	/**
	 * Fetches the root/home directory content.
	 * 
	 * @param sugarSync the cloud storage
	 */
	static void fetchHomeContent(SugarSync sugarSync) {
		Map<String, FileInfo> homeContents = new HashMap <String, FileInfo> ();
		try {
			HttpResponse httpResponse = new UserInfo().getUserInfo(sugarSync.getAccessToken());
			sugarSync.setCurrentHttpResponse(httpResponse);
			homeContents = SugarSyncUtils.getUserInfoAsMap(httpResponse);			
			sugarSync.setHomeContents(homeContents);
		} catch (IOException e) {
			e.printStackTrace();
		}		

	}
	
	

	/**
	 * Gets the next level in the directory tree based on the parent (contentLink) for the given 
	 * cloud storage.
	 * 
	 * @param contentLink the parent
	 * @param sugarSync the cloud storage
	 * @return a map of the files in the next level
	 */
	static Map<String, FileInfo> getNextLevel(String contentLink, SugarSync sugarSync) throws IOException{		
		HttpResponse httpResponse = SugarSyncHTTPGetUtil.getRequest(contentLink, sugarSync.getAccessToken());	
		sugarSync.setCurrentHttpResponse(httpResponse);
		Map<String, FileInfo> contents = SugarSyncUtils.getContentsRepresentationAsMap(httpResponse, true);
		sugarSync.setCurrentContents(contents);
		return contents;			
	}



	/**
	 * This method will fetch a file from sugarsync and save it to the server and 
	 * forward details about its location on the server and MIME type via a FileInfo
	 * object back to the caller.
	 * 
	 * @param contentLink the link to the resource on the sugersync server
	 * @return a FileInfo data object that contains info regarding the file requested.
	 * @throws IOException
	 */
	public FileInfo getFile(String displayName, String path, SugarSync sugarSync) throws IOException{		

		return SugarSyncUtils.getFile(displayName, path, sugarSync.getCurrentContents(), sugarSync.getAccessToken());

	}



	/**
	 * Opens an inputstream to the given file on the sugarsync servers
	 * 
	 * @param displayName
	 * @return
	 * @throws IOException
	 */
	public InputStream getInputStream(String displayName, SugarSync sugarSync) throws IOException{		

		return SugarSyncUtils.getInputStream(displayName, sugarSync.getCurrentContents(), sugarSync.getAccessToken());  

	}



}




