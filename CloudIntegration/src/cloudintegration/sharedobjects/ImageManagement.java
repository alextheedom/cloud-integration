package cloudintegration.sharedobjects;



	public class ImageManagement {
		
		
		
		/**
		 * Gets the image based on the type.
		 * 
		 * @param type the type of image requested
		 * @return the image file name
		 */
		public static String getImage(String type){
			
			String img = "default.png";
			switch (type){
				case "folder": 			img = "folder.png";
				break;
				case "file": 			img = "file.png";
				break;
				case "syncFolder": 		img = "folder.png";
				break;
				case "image": 			img = "image.png";
				break;
				case "webArchive": 		img = "webArchive.png";
				break;
				case "publicLinks": 	img = "folder.png";
				break;
				case "recentActivities": img = "folder.png";
				break;
				case "receivedShares": 	img = "receivedShares.png";
				break;
				case "magicBriefcase": 	img = "magicBriefcase.png";
				break;
				case "albums": 			img = "album.png";
				break;
				case "workspaces": 		img = "folder.png";
				break;
				case "mobilePhotos":	img = "folder.png";
				break;
				case "deleted": 		img = "deleted.png";
				break;
				case "film": 			img = "video.png";
				break;
				case "pdf": 			img = "pdf.png";
				break;
			}
			
			System.out.println("type: " + type + " - img: " + img);
			
			return img;
		}

		
		
		public static String getImageBasedOnExtension(String displayName) {
			
			int length = displayName.length();
			String ext = displayName.substring(length-4, length-1);
			
			
			String img = "default.png";
			
			switch(ext){
				case "pdf":	img = getImage("pdf");
				break;
				case "xml":	img = getImage("xml");
				break;
				case "zip":	img = getImage("zip");
				break;
				case "mpg":	img = getImage("audio");
				break;
				case "wma":	img = getImage("audio");
				break;
				case "jpeg":	img = getImage("image");
				break;
				case "jpg":	img = getImage("image");
				break;
				case "png":	img = getImage("image");
				break;
				case "tiff":	img = getImage("image");
				break;
				case "gif":	img = getImage("image");
				break;
				case "plain":	img = getImage("text");
				break;
				case "mpeg":	img = getImage("video");
				break;
				case "mp4":	img = getImage("video");
				break;
				case "mov":	img = getImage("video");
				break;
				
			}

			
			System.out.println("displayName: " + displayName + " - ext: " + ext + " - img: " + img);
			return img;
		}	
		
		/**
		 * Gets the image based on the MIME type of the file.
		 * 
		 * @param mediaType the MIME type of the file
		 * @return the image name
		 */
		public static String getImageBasedOnMime(String mediaType) {
			
			String img = "default.png";
			
			switch(mediaType){
				case "application/pdf":	img = getImage("pdf");
				break;
				case "application/xml":	img = getImage("xml");
				break;
				case "application/zip":	img = getImage("zip");
				break;
				case "audio/mp4":	img = getImage("audio");
				break;
				case "audio/mpeg":	img = getImage("audio");
				break;
				case "audio/vnd.wave":	img = getImage("audio");
				break;
				case "image/jpeg":	img = getImage("image");
				break;
				case "image/png":	img = getImage("image");
				break;
				case "image/tiff":	img = getImage("image");
				break;
				case "image/gif":	img = getImage("image");
				break;
				case "text/plain":	img = getImage("text");
				break;
				case "video/mpeg":	img = getImage("video");
				break;
				case "video/mp4":	img = getImage("video");
				break;
				case "video/quicktime":	img = getImage("video");
				break;
				
			}

			
			System.out.println("mediaType: " + mediaType + " - img: " + img);
			return img;
		}

	}
