package cloudintegration.sharedobjects;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Utils {
	
	private static final float KILOBYTE = 1024;
	private static final float MEGABYTE = 1048576;
	private static final float GIGABYTE = 1073741824;


	/**
	 * Converts the given file size into short hand
	 * 
	 * @param fileSize the size to convert
	 * @return the converted size
	 */
	public static String convertFileSize(String fileSize) {
		String rtn = "";

			float size = Float.parseFloat(fileSize);
			String units = "KB";
			if (size < MEGABYTE){
				size = size / KILOBYTE;
			} else if(size > MEGABYTE && size < GIGABYTE) {
				size = size / MEGABYTE;
				units = "MB";
			} else if (size > GIGABYTE){
				size = size / GIGABYTE;
				units = "GB";
				
			}
			
			DecimalFormat df = new DecimalFormat("###.##");
			df.setRoundingMode(RoundingMode.HALF_UP);
			
			rtn = "(" + df.format(size) + units + ")";
	
		
		return rtn;
	}

}
