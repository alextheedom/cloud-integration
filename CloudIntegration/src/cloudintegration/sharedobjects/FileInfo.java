package cloudintegration.sharedobjects;

import java.util.List;

import com.dropbox.client2.DropboxAPI.Entry;

public class FileInfo {
		
	private String displayName;
	private String size;
	private String mimeType;
	private String contentsLink;
	private String lastModified;
	private String icon;
	
	// SugarSync
	private String ref;
	
	// DropBox
	private String clientMtime;
	private List<Entry> contents;
	private String hash;
	private boolean isDeleted;
	private boolean isDir;
	private String path;
	private String rev;
	private String root;	
	private boolean thumbExists;
	
	
	
	/**
	 * Default Constructor
	 */
	public FileInfo(){}
	
	
	
	/**
	 * This is an object that represents the file downloaded from sugarsync.
	 * 
	 * @param displayName the file name
	 * @param size the file size
	 * @param locationOnServer the location of the server of the file
	 * @param mimeType the MIME type of the file
	 */
	public FileInfo(String displayName, String size , String mimeType){
		setDisplayName(displayName);
		setSize(size);
		setMimeType(mimeType);
	}
	
	
	
	/**
	 * Represents a simple file info object
	 * @param displayName the name of the file or directory
	 * @param contentsLink the link to the resource on the SugarSync servers
	 */
	public FileInfo(String displayName, String contentsLink){
		setDisplayName(displayName);
		setContentsLink(contentsLink);
	}
	
	
	
	/* Getters and setters */
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getContentsLink() {
		return contentsLink;
	}
	public void setContentsLink(String contentsLink) {
		this.contentsLink = contentsLink;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}


	public String getClientMtime() {
		return clientMtime;
	}


	public void setClientMtime(String clientMtime) {
		this.clientMtime = clientMtime;
	}


	public List<Entry> getContents() {
		return contents;
	}


	public void setContents(List<Entry> contents) {
		this.contents = contents;
	}


	public String getHash() {
		return hash;
	}


	public void setHash(String hash) {
		this.hash = hash;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public boolean isDir() {
		return isDir;
	}


	public void setDir(boolean isDir) {
		this.isDir = isDir;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public String getRev() {
		return rev;
	}


	public void setRev(String rev) {
		this.rev = rev;
	}


	public String getRoot() {
		return root;
	}


	public void setRoot(String root) {
		this.root = root;
	}


	public boolean isThumbExists() {
		return thumbExists;
	}


	public void setThumbExists(boolean thumbExists) {
		this.thumbExists = thumbExists;
	}








	

}
