package cloudintegration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import cloudintegration.sharedobjects.FileInfo;

public interface CloudStorage {

	public String authenticateApp() throws Exception;
	
	public CloudStorage login(String username, String password) throws Exception;
	
	public Map<String, FileInfo> getHomeDirectory() throws IOException;	
	
	public Map<String, FileInfo> getNextLevel(String contentLink) throws IOException;
	
	public FileInfo getFile(String displayName, String path) throws IOException;
	
	public InputStream getInputStream(String displayName) throws IOException;
	
	public String getName();
	
	
}
