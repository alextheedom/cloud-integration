package cloudintegration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.dropbox.client2.exception.DropboxException;

import cloudintegration.dropbox.DropBox;
import cloudintegration.sharedobjects.FileInfo;

import exceptions.InvalidLoginException;

public class CloudContext {
	
//	private Logger logger = null;
		
	Map<String, CloudStorage> cloudList = new HashMap<String, CloudStorage>();
	
//	{
//		logger = Logger.getRootLogger();
//		BasicConfigurator.configure();
//		logger.setLevel(Level.DEBUG);
//	}
	
	
	
	public String authenticateApp(String cloudStorageName) {
		
		CloudStorage cloudStorage = CloudStorageFactory.createInstanceByName(cloudStorageName);
		String url = null;
		try {
			url = cloudStorage.authenticateApp();
			addCloudStorage(cloudStorageName, cloudStorage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return url;
	}

	
	
	/**
	 * Attempts to login to the given cloud storage with the username and password.
	 * 
	 * @param username the username of the user
	 * @param password the password of the users account
	 * @param cloudStorage the cloud storage to login in to
	 * @return true if the login details are correct otherwise false
	 * @throws Exception 
	 */
	public boolean login(String username, String password, String cloudStorageName)  {
		
//		logger.debug("Enter login method");
		
		boolean success = false;
		
		try {
			// Check to see if we dont already have an object of this type for this user.
			CloudStorage cloudStorage = getCloudStorage(cloudStorageName);
			if (cloudStorage == null){
				cloudStorage = CloudStorageFactory.createInstanceByName(cloudStorageName);
			}
			cloudStorage = cloudStorage.login(username, password);
			// If login is not successful then an InvalidLoginException will be thrown 
			// and no object will be added to the array. The following code will be skipped.
			addCloudStorage(cloudStorageName, cloudStorage);
			success = true;
		} catch (InvalidLoginException exLogin) {
//			logger.fatal("LOGIN FAILED: ", exLogin);
			success = false;
		} catch (DropboxException exDropBox) {
			success = false;
		} catch (Exception exGen){
//			logger.fatal("LOGIN FAILED: ", exGen);
			success = false;
		}
		
//		logger.debug("Exit login method");
		return success;		
	}
	
	
	
	/**
	 * Obtains the home directory of the given cloud storage
	 * 
	 * @param cloudStorageName the cloud storage name
	 * @return the home directory as a hashmap
	 */
	public Map<String, FileInfo> getHomeDirectory(String cloudStorageName){
		
		CloudStorage cloudStorage = getCloudStorage(cloudStorageName);
		
		if (cloudStorage != null){
			try {
				return cloudStorage.getHomeDirectory();
			} catch (IOException e) {
				e.printStackTrace();
			}			
		} else {
			
		}

		return null;
	}
	
	


	public Map<String, FileInfo> getNextLevel(String contentLink, String cloudStorageName) {
		
		CloudStorage cloudStorage = getCloudStorage(cloudStorageName);
				
		if (cloudStorage != null){
			try {
				return cloudStorage.getNextLevel(contentLink);
			} catch (IOException e) {
				e.printStackTrace();
			}			
		} else {
			
		}
		
		return null;	
	}	
	
	
	
	/**
	 * Adds the newly created cloud storage to the registry.
	 * 
	 * @param cloudStorageName the cloud storage name
	 * @param cloudStorage the cloud storage to ass to the list
	 */
	private void addCloudStorage(String cloudStorageName, CloudStorage cloudStorage){	
		cloudList.put(cloudStorageName, cloudStorage);
	}
	
	
	
	/**
	 * Get a cloud storage object from the context based on the given name.
	 * 
	 * @param cloudStorageName the cloud storage name
	 * @return returns the cloud storage object requested or null if it is not found
	 */
	private CloudStorage getCloudStorage(String cloudStorageName ){	
		return cloudList.get(cloudStorageName);
	}






	
	
	
}
