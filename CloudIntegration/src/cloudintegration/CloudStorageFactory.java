package cloudintegration;

import cloudintegration.dropbox.DropBox;
import cloudintegration.sugarsync.SugarSync;

public class CloudStorageFactory {

	
	
	public static CloudStorage createInstanceByName(String name){
		
		CloudStorage cloudStorage = null;
		
		switch(name) {
		
		case "sugarsync": 
			cloudStorage = new SugarSync();
			break;
		case "dropbox":
			cloudStorage = new DropBox();
			break;
		}
		
		return cloudStorage;
		
	}
	
	
}
