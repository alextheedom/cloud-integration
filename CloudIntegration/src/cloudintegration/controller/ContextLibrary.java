package cloudintegration.controller;

import java.util.HashMap;
import java.util.Map;

import cloudintegration.CloudContext;

public class ContextLibrary {
	
	private static Map<String, CloudContext> library = new HashMap<String, CloudContext>();

	public static CloudContext getCloudContext(String userId) {
		return library.get(userId);
	}
	
	public static void addCloudContext(String userId, CloudContext cloudContext) {
		library.put(userId, cloudContext);
	}

}
