package cloudintegration.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cloudintegration.controller.actions.Action;
import cloudintegration.controller.actions.ActionFactory;
import exceptions.ActionFactoryException;

public class FrontController extends HttpServlet{
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	
	
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		
	    try {
	        Action action = ActionFactory.getAction(request);
	        String view = action.execute(request, response);
	        if (view.equals(request.getServletPath().substring(1))) {
	            request.getRequestDispatcher(view + ".jsp").forward(request, response);
	        } else {
	            response.sendRedirect(view + ".jsp"); // We'd like to fire redirect in case of a view change as result of the action (PRG pattern).
	        }
	       
	    } catch (ActionFactoryException exAction){
	    	
	    } catch (Exception e) {
	        throw new ServletException("Executing action failed.", e);
	    }
		
		
	}
	
	
	
}
