package cloudintegration.controller.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cloudintegration.CloudContext;
import cloudintegration.controller.ContextLibrary;
import cloudintegration.sharedobjects.FileInfo;

public class ShowContentsAction implements Action{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		Map<String, FileInfo> contents = null;
		
		String view = "errorpage";	
		HttpSession session = request.getSession();
		
		String cloudStorageName = request.getParameter("cloudStorageName");
		String contentLink = request.getParameter("contentLink");
		
		String userId = (String) session.getAttribute("userId");
		CloudContext cloudContext = ContextLibrary.getCloudContext(userId);	
		
		if(contentLink == null){			
			contents = cloudContext.getHomeDirectory(cloudStorageName);
		} else {			
			contents = cloudContext.getNextLevel(contentLink, cloudStorageName);			
		}
		
		
		if (contents != null) {		
			session.setAttribute("directory", contents);
			session.setAttribute("cloudStorageName", cloudStorageName);			
			view = "showcontents";
		} 
			
		return view;

	}

}
