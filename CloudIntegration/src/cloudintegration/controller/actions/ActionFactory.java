package cloudintegration.controller.actions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import exceptions.ActionFactoryException;

public class ActionFactory {
	
	
	static private Map<String, Action> actions = new HashMap<String, Action>();


	static {			
		actions.put("POST/applogin", 	new AppLoginAction());	
		actions.put("POST/login", 		new LoginAction());
		actions.put("GET/login", 		new LoginAction());
		actions.put("GET/showcontents", new ShowContentsAction());	
		actions.put("POST/authenticate", new AuthenticateAction());		
	}
	
	
	public static Action getAction(HttpServletRequest request) throws ActionFactoryException {
		
		String action = request.getServletPath();
		
		if (action != null || action.length() > 0){			
			//action = action.replace("/", "");
		} else {
			throw new ActionFactoryException();
		}
		
		return ActionFactory.actions.get(request.getMethod() + action);
	    
	}

}
