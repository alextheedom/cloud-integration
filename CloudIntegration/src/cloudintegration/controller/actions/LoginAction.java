package cloudintegration.controller.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cloudintegration.CloudContext;
import cloudintegration.controller.ContextLibrary;
import cloudintegration.sharedobjects.FileInfo;

public class LoginAction implements Action{

	/**
	 * This method executes the login method of the given cloud storage. It retrieves the context
	 * for the current user from the context library and calls the login method. A cloud storage 
	 * will be created based on the cloud storage name and its login method will be called. If login is
	 * successful its object will be added to the context for the current user.
	 */
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
				
		String cloudStorageName = request.getParameter("cloudStorageName");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		HttpSession session = request.getSession();
		
		
		String view = "accessdenied";
		String userId = (String) session.getAttribute("userId");
		CloudContext cloudContext = ContextLibrary.getCloudContext(userId);
		
		boolean success = cloudContext.login(username, password, cloudStorageName);
		
		if (success) {		
			session.setAttribute("cloudStorageName", cloudStorageName);		
			view = "controlpanel";
		} 
		
		return view;
	}

}
