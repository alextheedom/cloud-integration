package cloudintegration.controller.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cloudintegration.CloudContext;
import cloudintegration.controller.ContextLibrary;

public class AuthenticateAction implements Action {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String cloudStorageName = request.getParameter("cloudStorageName");
		HttpSession session = request.getSession();
				
		String view = "accessdenied";
		String userId = (String) session.getAttribute("userId");
		CloudContext cloudContext = ContextLibrary.getCloudContext(userId);
		
		String url = cloudContext.authenticateApp(cloudStorageName);
		
		if (url != null) {		
			session.setAttribute("cloudStorageName", cloudStorageName);	
			session.setAttribute("authenticated", "Not Authenticated");	
			session.setAttribute("url", url);	
			view = "authenticate";
		} 
		
		return view;
		
		
	}

}
