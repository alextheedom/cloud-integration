<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, cloudintegration.sharedobjects.FileInfo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Cloud Integration: Show contents for <%=session.getAttribute("cloudStorageName")%></title>

	<!-- Style sheet for the page layout and colours -->
	<link rel="stylesheet" href="/CloudIntegration/css/pagestyle.css" type="text/css" media="screen"/>
	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		
	<!-- JS for the loading text -->
	<script type="text/javascript" src="/CloudIntegration/js/loading.js"></script>

</head>
<body>

<h1 align="center">Directory Contents</h1>
<div class="container">
	<div id="content"><div id='loading'></div>
	<div id="gotoroot"><a href='showcontents?&cloudStorageName=<%=session.getAttribute("cloudStorageName")%>'><< Go to root</a></div>
	
	
		<div id="directory">
		<%
		Map<String, FileInfo> contents = (HashMap<String, FileInfo>)session.getAttribute("directory");

				for(String displayName : contents.keySet()){
					FileInfo fileInfo = contents.get(displayName);
					out.print("<div class='item'>");				
					out.print("<img src='/CloudIntegration/images/file/" + fileInfo.getIcon() + "' />   ");
					
					out.print("<a class='loading' href='showcontents");
					out.print("?contentLink=" + fileInfo.getContentsLink());
					out.print("&displayName=" + displayName);
					out.print("&cloudStorageName=" + session.getAttribute("cloudStorageName"));							
					out.print("'>" + displayName + "</a>");
					
					out.print(" (" + fileInfo.getSize() + ") - " + fileInfo.getLastModified() + " </div>");
				}
		%>
		</div>
    </div>
</div>
</body>
</html>