<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Cloud Integration: Control Panel</title>
	
	<!-- Style sheet for the page layout and colours -->
	<link rel="stylesheet" href="/CloudIntegration/css/pagestyle.css" type="text/css" media="screen"/>
	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
	

</head>
<body>


You are now logged into <%=session.getAttribute("cloudStorageName")%>. What would you like to do: <br><br>

<a href='showcontents?&cloudStorageName=<%=session.getAttribute("cloudStorageName")%>'>Show contents of cloud storage</a>



</body>
</html>