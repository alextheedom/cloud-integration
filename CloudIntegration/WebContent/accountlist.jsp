<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Cloud Integration: Control Panel</title>
	
	<!-- Style sheet for the page layout and colours -->
	<link rel="stylesheet" href="/CloudIntegration/css/pagestyle.css" type="text/css" media="screen"/>
	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>

	<!-- JS for the login box -->
	<script type="text/javascript" src="/CloudIntegration/js/login.js"></script>

</head>
<body>

<div class="post">
    <div class="btn-sign">
		<a href="#login-box-sugarsync" class="login-window">Sugarsync </a>
    </div>
</div>

<div id="login-box-sugarsync" class="login-popup">
	<a href="#" class="close"><img src="/CloudIntegration/images/loginbox/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
	<form method="post" class="signin" action="login">
		<fieldset class="textbox">
		
			<input name="cloudStorageName" value="sugarsync" type="hidden">
			
			<label class="username"> 
				<span>Username or email</span> 
				<input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Username" required>
			</label> 
			
			<label class="password"> 
				<span>Password</span> 
				<input id="password" name="password" value="" type="password" placeholder="Password" required>
			</label>

			<button class="submit button" type="submit">Sign in</button>

			<p>
				<a class="forgot" href="#">Forgot your password?</a>
			</p>

		</fieldset>
	</form>
</div>



	<form method="post" class="signin" action="authenticate">
		<fieldset class="textbox">	
			<input name="cloudStorageName" value="dropbox" type="hidden">		
			<button class="submit button" type="submit">Authenticate</button>
		</fieldset>
	</form>

	<form method="post" class="signin" action="login">
		<fieldset class="textbox">	
			<input name="cloudStorageName" value="dropbox" type="hidden">		
			<button class="submit button" type="submit">Sign in</button>
		</fieldset>
	</form>

</body>
</html>