<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


You must visit the URL below in order to authenticate this app to access your drop box.
<br>
<a href="<%= session.getAttribute("url") %>"><%= session.getAttribute("url") %></a>
<br>
authenticated: <%=request.getAttribute("authenticated") %>


<!-- the idea is that after the user has authenticated he returns to "http://localhost:8000/CloudIntegration/login"
and the page will go to login the user -->


</body>
</html>