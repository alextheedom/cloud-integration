<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Cloud Integration : Application Login</title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Cloud Integration: Show contents for <%=session.getAttribute("cloudStorageName")%></title>

	<!-- Style sheet for the page layout and colours -->
	<link rel="stylesheet" href="/CloudIntegration/css/pagestyle.css" type="text/css" media="screen"/>
	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		
	<!-- JS for the login box -->
	<script type="text/javascript" src="/CloudIntegration/js/login.js"></script>


</head>
<body>



	<form method="post" class="signin" action="applogin">
		<fieldset class="textbox">
		
			<input name="action" value="applogin" type="hidden">
			
			<label class="username"> 
				<span>Username or email</span> 
				<input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Username" required>
			</label> 
			
			<label class="password"> 
				<span>Password</span> 
				<input id="password" name="password" value="" type="password" placeholder="Password" required>
			</label>

			<button class="submit button" type="submit">Sign in</button>

			<p>
				<a class="forgot" href="#">Forgot your password?</a>
			</p>

		</fieldset>
	</form>





</body>
</html>